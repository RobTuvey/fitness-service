﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fitness.Service.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PingController
    {
        [HttpGet]
        public ActionResult<string> Ping()
        {
            return "pong";
        }
    }
}
